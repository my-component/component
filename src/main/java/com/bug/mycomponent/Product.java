/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bug.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author OS
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id= " + id + ", name= " + name + ", price= " + price + ", image= " + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Espresso 1", 40, "1.jpg"));
        list.add(new Product(2, "Espresso 2", 50, "1.jpg"));
        list.add(new Product(3, "Espresso 3", 55, "1.jpg"));
        list.add(new Product(4, "NumYen", 30, "2.jpg"));
        list.add(new Product(5, "Charthai 1", 25, "3.jpg"));
        list.add(new Product(6, "Charthai 2", 30, "3.jpg"));
        list.add(new Product(7, "Charthai 3", 35, "3.jpg"));
        return list;
    }

}
